var co = require('co');
var fetch = require('node-fetch');
var DWebp = require('cwebp').DWebp;
var TelegramBot = require('node-telegram-bot-api');

var token = '272574361:AAFE_CdVYgYy6iBikDz65uEiemKlIaxPsTQ';

var bot = new TelegramBot(token, {polling: true});

bot.on('message', function (msg) {
    var chatId = msg.chat.id;
    var sticker = msg.sticker;
    if (sticker) {
        co(function *(){
            var link = yield bot.getFileLink(msg.sticker.file_id);
            var res = yield fetch(link);
            var buf = yield res.buffer();
            var decoder = new DWebp(buf);
            decoder.toBuffer(function(err, buffer) {
                if (err) {
                    bot.sendMessage(chatId, err);
                    return;
                }
                bot.sendPhoto(chatId, buffer, {
                    caption: link,
                    disable_notification: true,
                    reply_to_message_id: msg.message_id
                });
            });
        });
    }
});